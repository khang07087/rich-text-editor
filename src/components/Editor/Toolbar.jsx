import { useLexicalComposerContext } from '@lexical/react/LexicalComposerContext'
import { mergeRegister } from '@lexical/utils';
import clsx from 'clsx';
import { $getSelection, $isRangeSelection, FORMAT_ELEMENT_COMMAND, FORMAT_TEXT_COMMAND } from 'lexical';
import React from 'react'
import { BsTypeBold, BsTypeItalic, BsTypeStrikethrough, BsTypeUnderline } from 'react-icons/bs';
import {AiOutlineAlignLeft, AiOutlineAlignCenter, AiOutlineAlignRight} from 'react-icons/ai'
import {CiTextAlignJustify} from 'react-icons/ci'
import {HiSave} from 'react-icons/hi'
import {RiUploadCloudLine} from 'react-icons/ri'

const EDITOR_NAMESPACE = "text-editor"

export default function Toolbar() {
    const [editor] = useLexicalComposerContext();
    const [isBold, setIsBold] = React.useState(false);
    const [isItalic, setIsItalic] = React.useState(false);
    const [isStrikeThrough, setIsStrikeThrough] = React.useState(false);
    const [isUnderline, setIsUnderline] = React.useState(false);
    const [isAlign, setIsAlign] = React.useState([true, false, false, false]);

    const updateToolBar = React.useCallback( () => {
        const selection = $getSelection();
        if($isRangeSelection(selection)){
            setIsBold(selection.hasFormat('bold'))
            setIsItalic(selection.hasFormat('italic'));
            setIsStrikeThrough(selection.hasFormat('strikethrough'))
            setIsUnderline(selection.hasFormat('underline'))
        }
    }, [editor]);

    React.useEffect(() => {
        return mergeRegister(
            editor.registerUpdateListener(({editorState}) => {
                editorState.read(() => {
                    updateToolBar();
                });
            })
        )
    }, [updateToolBar, editor])
    return (
        <div className = "absolute z-20 bottom-0 left-1/2 transform -translate-x-1/2 min-w-52 h-10 px-2 py-2 bg-[#1b2733] mb-4 space-x-2 flex items-center rounded-sm">
           <button className = {clsx(
            'toolbar-button',
            isBold ? 'bg-gray-700': 'bg-transparent'
           )}
                    onClick = {() => {
                        editor.dispatchCommand(FORMAT_TEXT_COMMAND, 'bold')

                    }}>
            <BsTypeBold className='icon-toolbar-button'/>
           </button>
           <button className = {clsx(
            'toolbar-button',
            isItalic ? 'bg-gray-700' : 'bg-transparent'
           )}
                    onClick = {() => {
                        editor.dispatchCommand(FORMAT_TEXT_COMMAND, 'italic')
                    }}>
            <BsTypeItalic className='icon-toolbar-button'/>
           </button>

           <button className = {clsx(
            'toolbar-button',
            isUnderline ? 'bg-gray-700' : 'bg-transparent'
           )}
                    onClick = {() => {
                        editor.dispatchCommand(FORMAT_TEXT_COMMAND, 'underline')

                    }}>
            <BsTypeUnderline className='icon-toolbar-button'/>
           </button>

           <button className = {clsx(
            'toolbar-button',
            isStrikeThrough ? 'bg-gray-700' : 'bg-transparent'
           )}
                    onClick = {() => {
                        editor.dispatchCommand(FORMAT_TEXT_COMMAND, 'strikethrough')
                    }}>
            <BsTypeStrikethrough className='icon-toolbar-button'/>
           </button>

           <span className='w-[1px] bg-gray-500 block h-full'/>

           <button className = {clsx(
                'toolbar-button',
                isAlign[0] ? 'bg-gray-700' : 'bg-transparent'
           )}
                    onClick = {() => {
                        editor.dispatchCommand(FORMAT_ELEMENT_COMMAND, 'left')
                        setIsAlign([true, false, false, false])
                    }}
                    >
            <AiOutlineAlignLeft className='icon-toolbar-button'/>
           </button>

           <button className = {clsx(
                'toolbar-button',
                isAlign[1] ? 'bg-gray-700' : 'bg-transparent'
           )}
                    onClick = {() => {
                        editor.dispatchCommand(FORMAT_ELEMENT_COMMAND, 'center')
                        setIsAlign([false, true, false, false])
                    }}
                    >
            <AiOutlineAlignCenter className='icon-toolbar-button'/>
           </button>

           <button className = {clsx(
                'toolbar-button',
                isAlign[2] ? 'bg-gray-700' : 'bg-transparent'
           )}
                    onClick = {() => {
                        editor.dispatchCommand(FORMAT_ELEMENT_COMMAND, 'right')
                        setIsAlign([false, false, true, false])
                    }}
                    >
            <AiOutlineAlignRight className='icon-toolbar-button'/>
           </button>

           <button className = {clsx(
                'toolbar-button',
                isAlign[3] ? 'bg-gray-700' : 'bg-transparent'
           )}
                    onClick = {() => {
                        editor.dispatchCommand(FORMAT_ELEMENT_COMMAND, 'right')
                        setIsAlign([false, false, false, true])
                    }}
                    >
            <CiTextAlignJustify className='icon-toolbar-button'/>
           </button>

           <span className='w-[1px] h-full bg-gray-500 block'/>

           <button className='toolbar-button'
                    onClick = {() => {
                        localStorage.setItem(EDITOR_NAMESPACE,JSON.stringify(editor.getEditorState().toJSON()))
                    }}                
                >
            <HiSave className = 'icon-toolbar-button'/>
           </button>

           <button className='toolbar-burron'>
             <RiUploadCloudLine className = 'icon-toolbar-button'/>
           </button>
        </div>
    )
}
