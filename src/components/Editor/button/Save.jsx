import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useLexicalComposerContext } from '@lexical/react/LexicalComposerContext'
import { mergeRegister } from '@lexical/utils';
import clsx from 'clsx';
import { $getSelection, $isRangeSelection, FORMAT_TEXT_COMMAND } from 'lexical';
import React from 'react'
import {BsFillSave2Fill} from 'react-icons/bs'


const EDITOR_NAMESPACE = "text-editor"

export default function Save() {
    const [editor] = useLexicalComposerContext();

    return (
        <button className ={clsx(
            'px-1 hover:bg-gray-700 transition-colors duration-100 ease-in rounded-sm',
        )}
            onClick={() => {
                localStorage.setItem(EDITOR_NAMESPACE, JSON.stringify(editor.getEditorState().toJSON()))
                console.log(editor.getEditorState().toJSON())
            }}>
                <BsFillSave2Fill className = 'text-white h-6 w-6'/>

        </button>
    )
}
