import React from 'react'
import clsx from 'clsx'
import {
  $getRoot,
  $getSelection,
} from 'lexical'

import {LexicalComposer} from '@lexical/react/LexicalComposer'
import {RichTextPlugin} from '@lexical/react/LexicalRichTextPlugin'
import {ContentEditable} from '@lexical/react/LexicalContentEditable'
import {OnChangePlugin} from '@lexical/react/LexicalOnChangePlugin'
import {HistoryPlugin} from '@lexical/react/LexicalHistoryPlugin'
import {TreeViewPlugin} from '@lexical/react/LexicalTreeView'
import Toolbar from './Toolbar'
function onChange(state){
  state.read(() => {
    const root = $getRoot();
    const selection = $getSelection;
    //console.log(selection);
  })
}

const EDITOR_NAMESPACE = "text-editor"
export default function Editor() {
  const content = localStorage.getItem(EDITOR_NAMESPACE);
  return (
    <div className='bg-white relative rounded-sm w-3/4 h-3/4 border-solid border-black border-2'>
      <LexicalComposer 
        initialConfig = {{
          namespace: EDITOR_NAMESPACE,
          editorState: content,
          theme: {
            paragraph: 'mb-1',
            rtl: 'text-right',
            ltr: 'text-left',
            text: {
              bold : 'font-bold',
              italic: 'italic',
              underline: 'underline',
              strikethrough:'line-through'
            }
          },
          onError(error){
            console.log(error)
          }
        }}
      >
      <Toolbar />
      <RichTextPlugin contentEditable={<ContentEditable className = "h-[450px] outline-none py-[15px] px-2.5 resize-none overflow-hidden text-ellipsis" />} 
                      placeholder={<div className="absolute top-[15px] left-[10px] pointer-events-none select-none">Now write something brilliant....</div>}/>
      <OnChangePlugin onChange={onChange} />
      <HistoryPlugin />
      </LexicalComposer>
    </div>
  )
}
