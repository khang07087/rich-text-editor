import Editor from "./components/Editor/Editor";
function App() {
  return (
    <div className="flex items-center justify-center flex-col">
      <div >Rich text editor</div>
      <Editor />
    </div>
  );
}

export default App;
